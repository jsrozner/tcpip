import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;

public class WindowBuffer {
  private TCPManager manager;
  private TCPSock sock;
  private LinkedList<Transport> unsentPackets;
  private LinkedList<Transport> sentPackets;
  private HashMap<Integer, Long> packetSendTime;

  private final int initialWindowSize = 10;
  private final long initialTimeout = 1000;
  // The effective write buffer size is maxPackets * MAX_PAYLOAD_SIZE
  private final int maxPackets = 50;

  private int currentWindowSize;
  private long currentTimeout;
  // Track timeouts
  private int lastTimeoutBase; // The base seqno of last timeout.
  private int timeoutCounter;  // The index of this timeout.
  private int nextExpectedAck;
  private boolean isStarted;

  // Track for 3x duplicate acks.
  private int lastAckedSeqNo;
  private int dupAckCount;

  public WindowBuffer(TCPManager man, TCPSock sock) {
    this.manager = man;
    this.sock = sock;
    this.unsentPackets = new LinkedList<Transport>();
    this.sentPackets = new LinkedList<Transport>();
    this.currentWindowSize = this.initialWindowSize;
    this.lastTimeoutBase = -1;
    this.packetSendTime = new HashMap<Integer, Long>();
    this.currentTimeout = this.initialTimeout;
    this.isStarted = false;
    this.lastAckedSeqNo = -1; // An invalid seqno.
    this.dupAckCount = 0;
    this.timeoutCounter = 0;
  }

  /**
   * Tell the buffer what ack to start with.
   * @param start
   */
  public void startWithSeqNo(int start) {
    this.isStarted = true;
    this.nextExpectedAck = start;
  }

  /**
   * Buffer a new packet for sending. Packet will be sent if there is space
   * in the window and otherwise queued.
   * @param transport
   * @return true if packet is successfully queued.
   */
  public boolean bufferNewPacket(Transport transport) {
    if (!this.isStarted) {
      this.manager.log(1, "Tried to buffer packet with unstarted " +
          "windowbuffer");
      return false;
    }

    if (!this.hasSpace()) {
      return false;
    }
    this.unsentPackets.add(transport);
    this.sendPacketsIfSpace();
    return true;
  }

  /**
   * Removes from window any sent packets with seqno < ackedSeqNo.
   * @param transport
   * @return true if any packets were removed from window.
   */
  public void gotNewAck(Transport transport) {
    if (!this.isStarted) {
      this.manager.log(1, "Ack received but window buffer not started!");
      return;
    }

    int ackedSeqNum = transport.getSeqNum();
    this.manager.log(4, "Got ack with seqnum " + ackedSeqNum);

    // Record duplicate acks.
    if (ackedSeqNum == this.lastAckedSeqNo) {
      this.dupAckCount++;
    } else if (ackedSeqNum > this.lastAckedSeqNo) {
      // Ignore older acks, but store new ack.
      this.lastAckedSeqNo = ackedSeqNum;
      this.dupAckCount = 0;
    }

    // If not a new ack then maybe adjust for congestion control.
    if (ackedSeqNum < this.nextExpectedAck) {
      System.out.print("?");  // Ack didn't advance ack field.
      this.congestionControlForDupAck();
      return;
    }

    // This is a new ack.
    System.out.print(":");

    // Expect next ack.
    this.nextExpectedAck = ackedSeqNum + 1;

    // Update timeout based on RTT.
    this.maybeUpdateTimeout(ackedSeqNum);

    // Remove any acked packets from the window.
    Transport t;
    while ((t = this.sentPackets.peek()) != null) {
      if (ackedSeqNum >= t.getSeqNum() + t.getPayload().length) {
        // No longer part of window.
        this.sentPackets.remove();
        this.removeFromPendingTimeouts(t);

        // Increase window size for a valid ack.
        this.currentWindowSize++;
        this.manager.log(3, "Window size increased by 1 to: " + this
            .currentWindowSize);
      } else {
        break;
      }
    }

    // Update window if there is congestion.
    int advertisedWindow = transport.getWindow();
    if (advertisedWindow < this.currentWindowSize) {
      // Don't let window size drop below 1.
      this.manager.log(3, "Advertised window too small. Halving window size");
      this.reduceWindow(this.currentWindowSize / 2);
    }

    // Send any packets if there is space in the window.
    this.sendPacketsIfSpace();
  }

  /**
   *
   * @return true if there are any unAcked packets or any unsent packets (i
   * .e. if the buffer is nonempty)
   */
  public boolean hasOutstandingPacket() {
    if(this.unsentPackets.size() == 0 && this.sentPackets.size() == 0) {
      return false;
    }

    // Otherwise there is an outstanding packet.
    Transport t = unsentPackets.peek();
    if (t != null) {
      this.manager.log(5, "Outstanding packet: " + t.getSeqNum());
    }
    return true;
  }

  /**
   *
   * @return true if a new packet can be buffered
   */
  public boolean hasSpace() {
    if (this.sentPackets.size() + this.unsentPackets.size() >=
        this.maxPackets) {
      return false;
    }
    return true;
  }

  private void reduceWindow(int newWindow) {
    this.currentWindowSize = Math.max(1, newWindow);
    this.manager.log(3, "Reduced window to: " + this.currentWindowSize);

    // Move packets around in queues.
    if (this.sentPackets.size() <= this.currentWindowSize) {
      return;
    }

    // Make a new sent queue with the first window elements of the old.
    LinkedList<Transport> newSentQueue = new LinkedList<Transport>();
    for (int i = 0; i < this.currentWindowSize; ++i) {
      newSentQueue.add(this.sentPackets.remove());
    }

    // Push the remaining sent elements onto the top of the unsent queue.
    this.unsentPackets.addAll(0, this.sentPackets);

    this.sentPackets = newSentQueue;
  }

  private void congestionControlForDupAck() {
    // Handle duplicate ack.
    if (this.dupAckCount != 3) {
      return;
    }

    // 3x duplicate ack, so do congestion control.
    this.manager.log(3, "Triple duplicate ack for: " + this.lastAckedSeqNo);
    this.dupAckCount = 0;

    // Halve window size.
    this.reduceWindow(this.currentWindowSize / 2);

    // Resend all packets in window.
    int i = 0;
    for (Transport t : this.sentPackets) {
      // Break if we have reached window size packets sent.
      this.sock.sendPayload(t);
      this.recordSendTime(t);
      System.out.print("!");  // Retransmission.
    }

    // Register new base timeout.
    this.registerTimeout();
  }

  private void recordSendTime(Transport transport) {
    int expectedAck = transport.getSeqNum() + transport.getPayload().length;
    this.packetSendTime.put(expectedAck, this.manager.now());
  }

  private void maybeUpdateTimeout(int receivedAck) {
    long now = this.manager.now();
    if (!this.packetSendTime.containsKey(receivedAck)) {
      return;
    }
    long sentTime = this.packetSendTime.remove(receivedAck);
    long RTT = now - sentTime;
    this.manager.log(4, "Old timeout was " + this.currentTimeout);
    this.currentTimeout = (long) (0.8 * this.currentTimeout + 0.4 * RTT);
    this.manager.log(3, "Updated timeout to " + this.currentTimeout);
  }

  private void removeFromPendingTimeouts(Transport transport) {
    int expectedAck = transport.getSeqNum() + transport.getPayload().length;
    this.packetSendTime.remove(expectedAck);
  }

  /**
   * Send any unsent packets that can fit into the window. Start a new base
   * timer if base has changed.
   */
  private void sendPacketsIfSpace() {
    // Send as many packets as window allows.
    while (this.sentPackets.size() < this.currentWindowSize
           && this.unsentPackets.size() > 0) {
      Transport packetToSend = this.unsentPackets.remove();
      this.sentPackets.add(packetToSend);
      this.recordSendTime(packetToSend);
      this.sock.sendPayload(packetToSend);
    }

    // If we sent a packet and the base changed. Update the base and set a
    // timeout.
    if (!this.sentPackets.isEmpty()
        && this.baseSeqNum() > this.lastTimeoutBase) {
      this.registerTimeout();
    }
  }

  /**
   * Register a timeout to resend the window. Invalidates previous timeout.
   */
  private void registerTimeout() {
    if (this.sentPackets.isEmpty()) {
      this.manager.log(1, "Timeout registered but buffer is empty");
      this.manager.log(1, "Unsent buffer has: " + this.unsentPackets.size());
      return; // This should never happen.
    }

    // Update the base.
    this.lastTimeoutBase = this.baseSeqNum();

    // Increment timeout counter.
    this.timeoutCounter++;

    this.manager.log(3, "Registering timeout with index: " + this
        .timeoutCounter + " for seqno: " + this.baseSeqNum());

    // Register a timeout.
    try {
      Method method = Callback.getMethod("gotTimeout", this,
          new String[] {"java.lang.Integer"});
      Callback cb = new Callback(method, this,
          new Object[] {this.timeoutCounter});
      this.manager.addTimer(this.currentTimeout, cb);
    } catch (Exception e) {
      this.manager.logError("Failed to add timer callback.");
      e.printStackTrace();
    }
  }

  public void gotTimeout(Integer timeout) {
    // Do nothing if we have registered a new timeout since this one.
    int timeoutIndex = timeout;
    if (this.timeoutCounter != timeoutIndex || this.sentPackets.isEmpty()) {
      this.manager.log(5, "Got an expired timeout with index: " +
          timeoutIndex);
      return;
    }

    // Reset triple duplicate acks.
    this.dupAckCount = 0;

    // Got a valid timeout. Drop window to size 1 and resend packet.
    this.manager.log(3, "Window timed out. Setting win size to 1. Timeout " +
        "index " + timeoutIndex);
    this.reduceWindow(1);

    Transport t = this.sentPackets.peek();
    this.recordSendTime(t);
    this.sock.sendPayload(t);
    System.out.print("!");
    this.registerTimeout();
  }

  private int baseSeqNum() {
    Transport basePacket = this.sentPackets.peek();
    if (basePacket == null) {
      return -1;    // This should never happen.
    }
    return basePacket.getSeqNum();
  }
}
