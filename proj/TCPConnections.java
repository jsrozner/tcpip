import java.util.HashMap;

/**
 * Manages TCPConnections.
 */
public class TCPConnections {
  // Connections are stored as <src addr:src port:dest addr:dest port>.
  private HashMap<String, TCPSock> connections;
  private TCPManager manager;

  public TCPConnections(TCPManager man) {
    this.manager = man;
    this.connections = new HashMap<String, TCPSock>();
  }

  private String fourTuple(int srcAddr, int srcPort, int destAddr,
                           int destPort) {
    return srcAddr + ":" + srcPort + ":" + destAddr + ":"
        + destPort;
  }

  public void registerConnection(int srcAddr, int srcPort, int destAddr,
                                 int destPort, TCPSock sock) {
    String fourTuple = fourTuple(srcAddr, srcPort, destAddr, destPort);
    if (this.connections.containsKey(fourTuple)) {
      this.manager.log(1, "Fourtuple already registered: " + fourTuple);
      return;
    }
    this.manager.log(4, "Registered connection: " + fourTuple);

    this.connections.put(fourTuple, sock);
  }

  public TCPSock lookUpSocketForPacket(Packet p) {
    Transport t = Transport.unpack(p.getPayload());
    String fourTuple = fourTuple(p.getSrc(), t.getSrcPort(), p.getDest(),
        t.getDestPort());
    if (this.connections.containsKey(fourTuple)) {
      return this.connections.get(fourTuple);
    }
    return null;
  }

  public void deregisterConnection(int srcAddr, int srcPort, int destAddr,
                                   int destPort) {
    String fourTuple = fourTuple(srcAddr, srcPort, destAddr, destPort);
    if (!this.connections.containsKey(fourTuple)) {
      this.manager.log(1, "Attempted to degister a nonexistent socket: " +
          fourTuple);
      return;
    }
    this.manager.log(4, "Degistered socket: " + fourTuple);
    this.connections.remove(fourTuple);
  }
}