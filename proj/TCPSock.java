import java.lang.reflect.Method;
import java.util.*;

/**
 * <p>Title: CPSC 433/533 Programming Assignment</p>
 * <p/>
 * <p>Description: Fishnet socket implementation</p>
 * <p/>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p/>
 * <p>Company: Yale University</p>
 *
 * @author Hao Wang
 * @version 1.0
 */

public class TCPSock {
  // TCP socket states
  enum State {
    CLOSED,
    LISTEN,
    SYN_SENT,
    ESTABLISHED,
    SHUTDOWN // close requested, FIN not sent (due to unsent data in queue)
  }
  private State state;

  enum Type {
    NONE,
    LISTEN,
    SERVER,
    CLIENT
  }
  private Type type;

  // A random starting seqno is chosen in the range 0 to maxStartSeqNo.
  private static final int maxStartingSeqNo = 10000;
  // Resend syns at rate synTimeout until connection is accepted.
  private static final int synTimeout = 1000;
  private static final byte dummy[] = new byte[0];
  // Effectively the server/ receiver buffer size.
  private static final int maxUnreadPackets = 100;

  private int boundPort;
  private boolean isBound;
  private TCPManager manager;      // For TCP communication.

  // For Listen sockets.
  private LinkedList<Packet> connectionQueue;
  private HashSet<String> pendingConnections; // For unaccepted connections.
  private int backlog;   // max number of outstanding conn in queue.

  // For read/write sockets.
  private int destAddr;
  private int destPort;

  // For write (client) sockets.
  private int nextSeqNoToQueue;
  private int synSeqNo;
  private boolean synSent;

  // For read (server) sockets.
  private LinkedList<Transport> unreadPackets;
  // In case we partially read a packet. Max size is MAX_PAYLOAD_SIZE.
  private byte[] unreadBytes;   // In case we partially read a packet.
  private int nextExpectedSeqNo;
  private WindowBuffer windowBuffer;

  /**
   * Public constructor called by TCPManager's socket().
   */
  public TCPSock(TCPManager manager) {
    this.state = State.CLOSED;  // Initially closed.
    this.type = Type.NONE;
    this.boundPort = -1;
    this.isBound = false;
    this.connectionQueue = new LinkedList<Packet>();
    this.manager = manager;
  }

  /**
   *  Internal constructor called on accept();
   *  Builds a server (reading) socket.
   */
  private TCPSock(TCPManager manager, int bindport, int destAddr,
                  int destPort, int syn) {
    this.state = State.ESTABLISHED;
    this.type = Type.SERVER;
    this.boundPort = bindport;
    this.isBound = true;
    this.manager = manager;
    this.connectionQueue = null;
    this.destAddr = destAddr;
    this.destPort = destPort;
    this.nextExpectedSeqNo = syn + 1;
    this.unreadPackets = new LinkedList<Transport>();
    this.unreadBytes = new byte[0];
    this.sendAck();
    this.manager.registerSocket(this.boundPort, this.destAddr, this.destPort,
                                this);
  }

  /**
   * Compute the current space remaining in a client's buffer.
   * @return the window size this client should advertise
   */
  private int computeBufferSpace() {
    int packetSpace = this.maxUnreadPackets - this.unreadPackets.size();
    return packetSpace * Packet.MAX_PAYLOAD_SIZE;
  }

  /**
   * Methods for handling received packets.
   */

  /**
   * Entrance point for any received packet.
   * @param packet
   */
  public void onReceive(Packet packet) {
    switch (this.state) {
      case CLOSED:
        this.sendFin();
        break;
      case LISTEN:
        handlePacketForConnectionSocket(packet);
        break;
      case SYN_SENT:
        handlePacketForSynSent(packet);
        break;
      case ESTABLISHED:
        handleDataPacket(packet);
        break;
      case SHUTDOWN:
        handleDataPacket(packet);
        break;
      default:
        this.manager.logError("Packet with invalid state received.");
    }
  }

  /**
   * For listen socket, if packet is a SYN, enqueue if there's space.
   * Otherwise silently drop it.
   * @param p
   */
  private void handlePacketForConnectionSocket(Packet p) {
    Transport t = Transport.unpack(p.getPayload());
    if (t.getType() != Transport.SYN
        || connectionQueue.size() == this.backlog) {
      // Silently drop.
      return;
    }

    // Make sure we don't have any outstanding syns with this client.
    String newConn = p.getSrc() + ":" + t.getSrcPort();
    if (this.pendingConnections.contains(newConn)) {
      this.manager.log(4, "Got additional syn from: " + newConn);
      return;
    }
    this.pendingConnections.add(newConn);
    System.out.print("S");
    connectionQueue.add(p);
  }

  /**
   * For a client socket in state SYN_SENT, looks for an ACK packet
   * with the correct seqno and changes to ESTABLISHED.
   * @param p
   */
  private void handlePacketForSynSent(Packet p) {
    Transport t = Transport.unpack(p.getPayload());
    if (t.getType() != Transport.ACK ||
        t.getSeqNum() != this.synSeqNo + 1) {
      // Silently drop packet.
      return;
    }

    this.state = State.ESTABLISHED;
    System.out.print(":");
  }

  private void handleDataPacket(Packet p) {
    if (this.type == Type.CLIENT) {
      handleClientDataPacket(p);
    } else if (this.type == Type.SERVER) {
      handleServerDataPacket(p);
    }
  }

  /**
   * Client data packets should be ACKs or FINs.
   * @param p
   */
  private void handleClientDataPacket(Packet p) {
    Transport t = Transport.unpack(p.getPayload());
    if (t.getType() == Transport.ACK) {
      // Handle the ack.
      this.windowBuffer.gotNewAck(t);

      // If we have no more packets to Ack and the socket was closed,
      // we can release now.
      if (this.state == State.SHUTDOWN &&
          !this.windowBuffer.hasOutstandingPacket()) {
        this.release();
      }
    } else if (t.getType() == Transport.FIN) {
      // Disable this socket.
      System.out.print("F");
      this.internalRelease();
    }
  }

  /**
   * Handle a data packet received for a server.
   * If it's the expected seqno, add to unreadPacket queue and increment
   * seqno.
   * Send an ack if it's a new or already received packet.
   * On FIN, put socket in state SHUTDOWN.
   * @param p
   */
  private void handleServerDataPacket(Packet p) {
    Transport t = Transport.unpack(p.getPayload());
    this.manager.log(4, "Server data packet with seqno " + t.getSeqNum());
    this.manager.log(4, "Expected seqno " + this.nextExpectedSeqNo);

    // Drop packet if server is in state SHUTDOWN.
    if (this.state == State.SHUTDOWN) {
      this.manager.log(4, "Dropping packet. Already in state shutdown");
      return;
    }

    // Ack an additional SYN.
    if (t.getType() == Transport.SYN) {
      this.manager.log(4, "Additional syn received");
      this.sendAck();
      return;
    }

    // Go into shutdown on FIN to allow final reads.
    if (t.getType() == Transport.FIN) {
      System.out.print("F");
      this.state = State.SHUTDOWN;
      return;
    }

    // Handle a data packet.
    if (t.getType() == Transport.DATA) {
      if (t.getSeqNum() == this.nextExpectedSeqNo) {

        // If buffer is full, just send ack.
        if (this.unreadPackets.size() == this.maxUnreadPackets) {
          this.sendAck();
          return;
        }

        // New data packet to store in unreadPacket queue.
        System.out.print(".");
        this.unreadPackets.add(t);
        this.nextExpectedSeqNo += t.getPayload().length;
        this.sendAck();
      } else if (t.getSeqNum() < this.nextExpectedSeqNo) {
        // Duplicate packet
        System.out.print("!");
        this.sendAck();
      } else {
        // Future packet. Send ack.
        this.sendAck();
      }
    }
  }

  /*
   * The following are the socket APIs of TCP transport service.
   * All APIs are NON-BLOCKING.
   */

  /**
   * Bind a socket to a local port
   *
   * @param localPort int local port number to bind the socket to
   * @return int 0 on success, -1 otherwise
   */
  public int bind(int localPort) {
    if (this.state != State.CLOSED || this.isBound) {
      this.manager.logError("Socket is already bound to " + this.boundPort);
      return -1;
    }
    this.manager.log(3, "Bound to " + localPort);
    this.boundPort = localPort;
    this.isBound = true;
    return 0;
  }

  /**
   * Listen for connections on a socket
   *
   * @param backlog int Maximum number of pending connections
   * @return int 0 on success, -1 otherwise
   */
  public int listen(int backlog) {
    if (this.state != State.CLOSED || !this.isBound) {
      this.manager.logError("Socket is in invalid state to listen.");
      return -1;
    }
    if (!this.manager.registerListenSocket(this, this.boundPort)) {
      this.manager.logError("Port " + this.boundPort + " is already in use" +
          ".");
      return -1;
    }
    this.backlog = backlog;
    this.state = State.LISTEN;
    this.type = Type.LISTEN;
    this.pendingConnections = new HashSet<String>();
    return 0;
  }

  /**
   * Accept a connection on a socket
   *
   * @return TCPSock The first established connection on the request queue
   *
   */
  public TCPSock accept() {
    if (this.connectionQueue.isEmpty()) {
      return null;
    }
    Packet p = connectionQueue.remove();
    Transport in = Transport.unpack(p.getPayload());

    // Remove this source from pendingConnections.
    String newConn = p.getSrc() + ":" + in.getSrcPort();
    this.pendingConnections.remove(newConn);

    int syn = in.getSeqNum();

    // Make a new TCPSock bound to this port for reading.
    // Constructor will ACK seqNo.
    return new TCPSock(this.manager, this.boundPort, p.getSrc(),
        in.getSrcPort(), syn);
  }

  /**
   * Initiate connection to a remote socket (this is a client socket).
   * Will initialize windowBuffer with call to sendsyn().
   *
   * @param destAddr int Destination node address
   * @param destPort int Destination port
   * @return int 0 on success, -1 otherwise
   */
  public int connect(int destAddr, int destPort) {
    if (!this.isBound) {
      this.manager.logError("Socket is not bound! Can't connect");
      return -1;
    }
    if (!manager.registerClientSocket(this.boundPort, destAddr,
        destPort, this)) {
      this.manager.logError("A client socket is already registered at " + this
          .boundPort);
      return -1;
    }
    this.type = Type.CLIENT;
    this.destAddr = destAddr;
    this.destPort = destPort;
    this.windowBuffer = new WindowBuffer(this.manager, this);
    this.state = State.SYN_SENT;
    this.sendSyn();
    return 0;
  }


  /**
   * Initiate closure of a connection (graceful shutdown)
   */
  public void close() {
    this.manager.log(2, "Call to close socket");
    // If this is a listen or server socket, we just release it.
    if (this.type == Type.SERVER || this.type == Type.LISTEN) {
      this.release();
      return;
    }

    // For a client socket, if all packets have been read, we can just release.
    if (!this.windowBuffer.hasOutstandingPacket()) {
      this.release();
      return;
    }

    // Otherwise, we go into state shutdown to allow graceful propagation of
    // outstanding packets.
    this.manager.log(2, "Some packets outstanding, " +
        "going into state shutdown");
    this.state = State.SHUTDOWN;
  }


  /**
   * Release a connection immediately (abortive shutdown)
   */
  public void release() {
    if (this.state == State.CLOSED) {
      this.manager.log(1, "Release called but socket in state " +
          "closed");
      return;
    }

    if (this.type == Type.CLIENT || this.type == Type.SERVER) {
      this.sendFin();
    }

    this.manager.log(2, "Releasing socket");
    this.internalRelease();
  }

  /**
   * Private release function. Frees port and deregisters the socket but does
   * not sendFin().
   */
  private void internalRelease() {
    this.manager.log(2, "Freeing port, setting state closed, " +
        "and deregistering socket");
    this.freePort();
    this.state = State.CLOSED;
    manager.deregisterSocket(this.boundPort, this.destAddr, this.destPort);
  }

  private void freePort() {
    if (this.type == Type.LISTEN) {
      this.manager.deregisterListenSocket(this.boundPort);
    } else if (this.type == Type.CLIENT) {
      this.manager.deregisterClientSocket(this.boundPort);
    }
    this.isBound = false;
  }

  /**
   * Write to the socket up to len bytes from the buffer buf starting at
   * position pos.
   *
   * @param buf byte[] the buffer to write from
   * @param pos int starting position in buffer
   * @param len int number of bytes to write
   * @return int on success, the number of bytes written, which may be smaller
   *         than len; on failure, -1
   */
  public int write(byte[] buf, int pos, int len) {
    if (this.type != Type.CLIENT) {
      return -1;
    }
    if (this.state == State.CLOSED ||
        this.state == State.SHUTDOWN) {
      this.manager.logError("Socket has been closed or shutdown");
      return -1;
    }
    int curr_pos = pos;
    int total_bytes_written = 0, bytesWritten;
    while (this.windowBuffer.hasSpace() && total_bytes_written < len) {
      bytesWritten = makePacket(buf, curr_pos, len - total_bytes_written);
      curr_pos += bytesWritten;
      total_bytes_written += bytesWritten;
    }
    return total_bytes_written;
  }

  /**
   * Write up to len bytes from buf starting at position pos to a packet and
   * send the packet.
   *
   * @param buf
   * @param pos
   * @param len
   * @return the number of bytes written (never more than MAX_PAYLOAD_SIZE
   */
  private int makePacket(byte[] buf, int pos, int len) {
    int bytesToSend = Math.min(Transport.MAX_PAYLOAD_SIZE, len);
    byte[] send = Arrays.copyOfRange(buf, pos, pos + bytesToSend);

    // Make a packet.
    Transport t = new Transport(this.boundPort, this.destPort,
        Transport.DATA, 0, this.nextSeqNoToQueue,
        send);
    this.nextSeqNoToQueue += send.length;

    if (this.windowBuffer.bufferNewPacket(t)) {
      return bytesToSend;
    }

    // Failed to buffer new packet.
    return 0;
  }

  public void sendPayload(Transport transport) {
    this.manager.log(3, "Sending packet with seqnum " + transport.getSeqNum());
    this.manager.packAndSendPayload(this.destAddr, transport);
  }

 /**
   * Read from the socket up to len bytes into the buffer buf starting at
   * position pos.
   *
   * @param buf byte[] the buffer
   * @param pos int starting position in buffer
   * @param len int number of bytes to read
   * @return int on success, the number of bytes read, which may be smaller
   *         than len; on failure, -1
   */
  public int read(byte[] buf, int pos, int len) {
    if (this.state == State.CLOSED) {
      this.manager.logError("Attempt to read from closed socket");
      return -1;
    }
    if (this.type != Type.SERVER) {
      this.manager.logError("Attempt to read from a receiving socket");
      return -1;
    }

    int bytesRead = 0;
    int bytesToRead;

    // Get any previously unread bytes.
    bytesToRead = Math.min(this.unreadBytes.length, len);
    System.arraycopy(this.unreadBytes, 0, buf, pos, bytesToRead);
    bytesRead += bytesToRead;

    // If we read fewer than the outstanding bytes, stop.
    if (bytesRead < this.unreadBytes.length) {
      this.unreadBytes = Arrays.copyOfRange(this.unreadBytes, bytesRead,
          this.unreadBytes.length);
      return bytesRead;
    }

    // Read packets until no more packets or until all bytes are read.
    byte[] incoming_bytes = new byte[0];
    bytesToRead = 0;
    while (bytesRead < len && this.unreadPackets.size() > 0) {
      Transport t = this.unreadPackets.remove();
      incoming_bytes = t.getPayload();
      bytesToRead = Math.min(incoming_bytes.length, len - bytesRead);
      System.arraycopy(incoming_bytes, 0, buf, pos + bytesRead, bytesToRead);
      bytesRead += bytesToRead;
    }

    // If we didn't read all of this packet, save it for later.
    if (bytesToRead < incoming_bytes.length) {
      this.unreadBytes = Arrays.copyOfRange(incoming_bytes, bytesToRead,
          incoming_bytes.length);
    } else {
      // Otherwise we have no unread bytes.
      this.unreadBytes = new byte[0];
    }

    // If we didn't get any bytes and we're in state shutdown,
    // then we can release this socket.
    if (this.state == State.SHUTDOWN && len > 0 && bytesRead == 0) {
      this.internalRelease();
    }
    return bytesRead;
  }

  /**
   * Private send functionalities.
   */

  /**
   * For a client socket,
   * Generate a random seqno and send a syn.
   * Set the expected ACK to seqno + 1.
   */
  public void sendSyn() {
    // Make sure we're still in state SYN_SENT in case this was called by a
    // syn timeout and we've already gotten the ack.
    if (this.state != State.SYN_SENT) {
      return;
    }

    // If we haven't already chosen a syn, choose one now.
    if (!this.synSent) {
      Random random = new Random();
      // We expect random + 1 for ack.
      this.synSeqNo = random.nextInt(this.maxStartingSeqNo);

      // The first ack we expect for data is >= syn + 1.
      this.windowBuffer.startWithSeqNo(this.synSeqNo + 1);

      // Set the seqno that we'll send.
      this.nextSeqNoToQueue = this.synSeqNo + 1;

      this.synSent = true;
    }

    // Otherwise, syn was dropped and we need to resend.
    Transport out = new Transport(this.boundPort, this.destPort,
        Transport.SYN, 0, this.synSeqNo, this.dummy);
    this.manager.log(4, "Sending syn with seqno " + this.synSeqNo);
    this.manager.packAndSendPayload(this.destAddr, out);
    System.out.print("S");

    // Register a callback to resend this syn in case its dropped.
    try {
      Method method = Callback.getMethod("sendSyn", this, null);
      Callback cb = new Callback(method, this, null);
      this.manager.addTimer(this.synTimeout, cb);
    } catch (Exception e) {
      this.manager.logError("Failed to add syn timer callback");
    }
  }

  /**
   * Server: send an ack and increment the expectedAck.
   */
  private void sendAck() {
    Transport out;
    out = new Transport(this.boundPort, this.destPort,
        Transport.ACK, this.computeBufferSpace(), this.nextExpectedSeqNo,
        this.dummy);
    this.manager.log(4, "Sending ack with seqno " + this.nextExpectedSeqNo);
    this.manager.packAndSendPayload(this.destAddr, out);
  }

  /**
   * Send a FIN packet.
   */
  private void sendFin() {
    Transport out = new Transport(this.boundPort, this.destPort,
        Transport.FIN, 0, 0, this.dummy);
    this.manager.log(4, "Sending fin.");
    this.manager.packAndSendPayload(this.destAddr, out);
    System.out.print("F");
  }

  /**
   * Socket state queries.
   */

  public boolean isConnectionPending() {
    return (state == State.SYN_SENT);
  }

  public boolean isClosed() {
    return (state == State.CLOSED);
  }

  public boolean isConnected() {
    return (state == State.ESTABLISHED);
  }

  public boolean isClosurePending() {
    return (state == State.SHUTDOWN);
  }
}
