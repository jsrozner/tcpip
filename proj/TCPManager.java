import java.lang.String;
import java.util.HashMap;
import java.util.HashSet;

/**
 * <p>Title: CPSC 433/533 Programming Assignment</p>
 * <p/>
 * <p>Description: Fishnet TCP manager</p>
 * <p/>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p/>
 * <p>Company: Yale University</p>
 *
 * @author Hao Wang
 * @version 1.0
 */
public class TCPManager {
  private Node node;
  private int addr;
  private Manager manager;

  private static final byte dummy[] = new byte[0];
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_YELLOW = "\u001B[33m";
  private static final String ANSI_RESET = "\u001B[0m";

  private TCPConnections tcpConnections;
  private HashMap<Integer, TCPSock> listenSockets;
  private HashSet<Integer> clientSockets;

  // Additional command line arguments.
  private final int maxDebugPriority = 4;
  private final boolean debug = false;

  public TCPManager(Node node, int addr, Manager manager) {
    this.node = node;
    this.addr = addr;
    this.manager = manager;
    this.tcpConnections = new TCPConnections(this);
    this.listenSockets = new HashMap<Integer, TCPSock>();
    this.clientSockets = new HashSet<Integer>();
  }

  /**
   *
   * @return current time in milliseconds
   */
  public long now() {
    return this.manager.now();
  }

  public void addTimer(long timeout, Callback cb) {
    this.manager.addTimer(this.addr, timeout, cb);
  }

  private void log(String msg) {
    System.out.println(msg);
  }

  public void log(int priority, String msg) {
    if (!this.debug || priority > this.maxDebugPriority) {
      return;
    }
    switch (priority) {
      case 1:
        System.out.println(ANSI_RED + msg + ANSI_RESET);
        break;
      case 2:
        System.out.println(ANSI_YELLOW + msg + ANSI_RESET);
        break;
      default:
        this.log(msg);
    }
  }

  public void logError(String msg) {
    System.err.print(msg);
  }
  /**
   * Start this TCP manager
   */
  public void start() {
  }

    /*
     * Begin socket API
     */

  /**
   *
   * @param packet
   */
  public void onReceive(Packet packet) {
    // If 4-tuple is present, map to that socket.
    TCPSock sock = tcpConnections.lookUpSocketForPacket(packet);
    if (sock != null) {
      sock.onReceive(packet);
      return;
    }
    // Otherwise if it's a SYN, try to map to a connection socket.
    Transport t = Transport.unpack(packet.getPayload());
    if (t.getType() != Transport.SYN ||
        !this.listenSockets.containsKey(t.getDestPort())) {
      // Not a SYN so just a bad packet or else port isn't listening.
      sendFinReply(packet);
      return;
    }
    // Valid SYN, so map to listen socket.
    sock = this.listenSockets.get(t.getDestPort());
    sock.onReceive(packet);
    return;
  }
  /**
   * Create a socket
   *
   * @return TCPSock the newly created socket, which is not yet bound to
   *         a local port
   */
  public TCPSock socket() {
    return new TCPSock(this);
  }

    /*
     * End Socket API
     */

  /**
   * Interface to node's sendSegment command to be called by
   * TCPSock to transfer packets.
   */

  public void sendFinReply(Packet p) {
    Transport in = Transport.unpack(p.getPayload());
    Transport out =
        new Transport(in.getDestPort(), in.getSrcPort(), Transport.FIN, 0, 0,
                      dummy);
    this.packAndSendPayload(p.getSrc(), out);
  }

  public void packAndSendPayload(int dest, Transport t) {
    this.node.sendSegment(this.addr, dest, Protocol.TRANSPORT_PKT, t.pack());
  }

  /*
  Track active ports.
   */

  /**
   * Called by socket on listen to register itself as a listening socket.
   * @param sock
   * @param port
   * @return true if successfully registers.
   */
  public boolean registerListenSocket(TCPSock sock, int port) {
    if (this.listenSockets.containsKey(port)) {
      // Already listening on this socket.
      return false;
    }
    this.listenSockets.put(port, sock);
    return true;
  }

  /**
   * Called by a listen socket on destruction to free its port.
   * @param port
   */
  public void deregisterListenSocket(int port) {
    this.listenSockets.remove(port);
  }

  public boolean registerClientSocket(int boundport, int destAddr,
                                      int destPort, TCPSock sock) {
    if (this.clientSockets.contains(boundport)) {
      // Already have a client on this port.
      return false;
    }
    this.clientSockets.add(boundport);
    tcpConnections.registerConnection(destAddr, destPort, this.addr,
        boundport, sock);
    return true;
  }

  public void deregisterClientSocket(int port) {
    this.clientSockets.remove(port);
  }

  public void deregisterSocket(int boundport, int destAddr, int destPort) {
    this.tcpConnections.deregisterConnection(destAddr, destPort, this.addr,
        boundport);
  }
  public void registerSocket(int boundport, int destAddr, int destPort,
                             TCPSock sock) {
    this.tcpConnections.registerConnection(destAddr, destPort, this.addr,
        boundport, sock);
  }
}
